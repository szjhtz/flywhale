<?php

namespace app\service;

use app\model\Dict;
use app\validate\DictValidate;
use think\annotation\Inject;
use think\exception\ValidateException;

class DictService
{
    /**
     * @Inject()
     * @var Dict
     */
    protected $dictModel;

    public function getDictList($param)
    {
        $limit = $param['pageSize'];

        $where = [];
        if (!empty($param['id'])) {
            $where[] = ['cate_id', '=', $param['id']];
        }

        return $this->dictModel->getDictList($limit, $where);
    }

    /**
     * 添加字典
     * @param $param
     * @return array
     */
    public function addDict($param)
    {
        try {

            validate(DictValidate::class)->check($param);
        } catch (ValidateException $e) {
            return dataReturn(-3, $e->getError());
        }

        return $this->dictModel->addDict($param);
    }

    /**
     * 编辑字典
     * @param $param
     * @return array
     */
    public function editDict($param)
    {
        try {

            validate(DictValidate::class)->check($param);
        } catch (ValidateException $e) {
            return dataReturn(-3, $e->getError());
        }

        return $this->dictModel->editDict($param);
    }

    /**
     * 删除字典
     * @param $id
     * @return array
     */
    public function delDict($id)
    {
        return $this->dictModel->delDict($id);
    }

    /**
     * 根据分类id获取分类信息
     * @param $cateId
     * @return array
     */
    public function getInfoByCateId($cateId)
    {
        return $this->dictModel->getDictByCateId($cateId);
    }
}