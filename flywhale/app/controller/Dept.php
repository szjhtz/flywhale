<?php

namespace app\controller;

use app\service\DeptService;
use think\annotation\Inject;

class Dept extends Base
{
    /**
     * @Inject()
     * @var DeptService
     */
    protected $deptService;

    public function index()
    {
        $res = $this->deptService->getDeptList();
        return json($res);
    }

    public function add()
    {
        if (request()->isPost()) {

            $param = input('post.');

            $res = $this->deptService->addDept($param);
            return json($res);
        }
    }

    public function edit()
    {
        if (request()->isPost()) {

            $param = input('post.');

            $res = $this->deptService->editDept($param);
            return json($res);
        }
    }

    public function del()
    {
        $id = input('param.id');

        $res = $this->deptService->delDept($id);
        return json($res);
    }
}