<?php
/**
 * Created by PhpStorm.
 * Date: 2022/4/18
 * Time: 21:12
 */
namespace app\validate;

use think\Validate;

class FlowValidate extends Validate
{
    protected $rule = [
        'link_type|关联类型' => 'require',
        'name|流程名称' => 'require'
    ];
}