import http from "@/utils/request"

export default {
	list: {
		url: '/form/index',
		name: "获取表单列表",
		get: async function (data = {}) {
			return await http.get(this.url, data);
		}
	},
	add: {
		url: '/form/add',
		name: "添加表单",
		post: async function (data = {}) {
			return await http.post(this.url, data);
		}
	},
	edit: {
		url: '/form/edit',
		name: "编辑表单",
		post: async function (data = {}) {
			return await http.post(this.url, data);
		}
	},
	del: {
		url: '/form/del',
		name: "删除表单",
		get: async function (data = {}) {
			return await http.get(this.url, data);
		}
	},
	deploy: {
		url: '/form/deploy',
		name: "部署表单",
		post: async function (data = {}) {
			return await http.post(this.url, data);
		}
	},
	undeploy: {
		url: '/form/undeploy',
		name: "卸载表单",
		get: async function (data = {}) {
			return await http.get(this.url, data);
		}
	},
	getForm: {
		url: '/form/allForm',
		name: "获取表单",
		get: async function (data = {}) {
			return await http.get(this.url, data);
		}
	},
}